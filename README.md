# TP Front BRUN-GIGLIO Alexandre B3 Info

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.3.

## Install 
Run `npm install`

## Development server

Run `ng serve` or `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Only with Google Chrome

## Check List 
- [x] Consulter un tableau paginé des données. 
- [x] Visualiser les données sous forme de graphiques (camembert & histogramme).
- [x] Effectuer une recherche dans le tableau de données. 
- [x] Filtrer les données en utilisant un formulaire simple. 
- [x] Consulter le détail de chaque ligne du tableau et modifier les données. 
- [x] Exporter les données modifiées sous forme de JSON. 

BONUS : 
- [ ] Filter les données en cliquant sur les graphiques. 
- [x] Visualiser les données sur une carte géographique. (In Progress)
- [x] Implémenter des test unitaires & end to end (e2e). 

