import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../data-api.service';
import { People } from '../people';

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss']
})
export class MapViewComponent implements OnInit {
  title = 'My first AGM project';
  lat = 51.678418;
  lng = 7.809007;

  peoples: People[] = [];

  constructor(
    private apiService: DataApiService,
  ) { }

  ngOnInit(): void {
    this.init();
  }

  async init() {
    const getAllPeople = await this.apiService.getPeoples().toPromise();
    if (getAllPeople.people)
      this.peoples = getAllPeople.people;
  }

}
