import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { People } from '../people';

@Component({
  selector: 'app-json-dialog',
  templateUrl: './json-dialog.component.html',
  styleUrls: ['./json-dialog.component.scss']
})
export class JsonDialogComponent implements OnInit {
  downloadJsonHref: {};
  private setting = {
    element: {
      dynamicDownload: null as HTMLElement
    }
  }
  constructor(
    public dialogRef: MatDialogRef<JsonDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public peopleData: {},
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit(): void {
    console.log("🚀 ~ file: json-dialog.component.ts ~ line 16 ~ JsonDialogComponent ~ peopleData", this.peopleData)
  }

  dynamicDownloadJson() {
    this.dyanmicDownloadByHtmlTag({
      fileName: 'PeopleData.json',
      text: JSON.stringify(this.peopleData)
    });
  }

  private dyanmicDownloadByHtmlTag(arg: {
    fileName: string,
    text: string
  }) {
    if (!this.setting.element.dynamicDownload) {
      this.setting.element.dynamicDownload = document.createElement('a');
    }
    const element = this.setting.element.dynamicDownload;
    const fileType = arg.fileName.indexOf('.json') > -1 ? 'text/json' : 'text/plain';
    element.setAttribute('href', `data:${fileType};charset=utf-8,${encodeURIComponent(arg.text)}`);
    element.setAttribute('download', arg.fileName);

    let event = new MouseEvent("click");
    element.dispatchEvent(event);
  }
}
