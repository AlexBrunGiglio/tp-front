export class PeopleList {
    people: People[];
}

export class People {
    contact: {
        email: string;
        address: string;
        city: string;
        country: string;
        location: {
            lon: number;
            lat: number;
        };
        phone: string;
    };
    firstname: string;
    gender: string;
    id: number;
    lastname: string;
    preferences: {
        favorite_color: string;
        favorite_fruit: string;
        favorite_movie: string;
        favorite_pet: string;
    }
}
