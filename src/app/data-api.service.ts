import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { People, PeopleList } from './people';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {

  apiUrl: string = 'https://run.mocky.io/v3/70e5b0ad-7112-41c5-853e-b382a39e65b7'

  constructor(
    private httpClient: HttpClient
  ) { }

  public getPeoples() {
    const response = this.httpClient.get<PeopleList>(`${this.apiUrl}`);
    console.log("🚀 ~ file: data-api.service.ts ~ line 17 ~ DataApiService ~ getPeoples ~ response", response)
    return response;
  }

  public getPeople(id: number) {
  }
}
