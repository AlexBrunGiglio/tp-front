import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { People } from '../people';
import { } from 'googlemaps';


@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  @ViewChild('map', { static: true }) mapElement: any;
  map: google.maps.Map;
  isEditMode: boolean = false;
  genderOptions = {
    female: "Female",
    male: "Male"
  };
  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public peopleData: People,
  ) { }

  ngOnInit(): void {
    this.isEditMode = false;

  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.initMap();
    // const mapProperties = {
    //   center: new google.maps.LatLng(this.peopleData.contact.location.lat, this.peopleData.contact.location.lon),
    //   zoom: 15,
    //   mapTypeId: google.maps.MapTypeId.ROADMAP
    // };

    // this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
  }

  initMap(): void {
    const myLatLng = { lat: this.peopleData.contact.location.lat, lng: this.peopleData.contact.location.lon };

    const map = new google.maps.Map(
      this.mapElement.nativeElement,
      {
        zoom: 4,
        center: myLatLng,
      }
    );

    new google.maps.Marker({
      position: myLatLng,
      map,
      title: "Marker",
    });
  }

  changeToEditMode(isEdit: boolean = false) {
    if (isEdit == true) {
      this.onCloseDialog();
    } else {
      this.isEditMode = true;
    }
  }
  onCloseDialog() {
    this.dialogRef.close(this.peopleData);
  }
}
