import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChartsComponent } from './charts/charts.component';
import { HomeComponent } from './home/home.component';
import { MapViewComponent } from './map-view/map-view.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'charts', component: ChartsComponent },
  { path: 'maps', component: MapViewComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
