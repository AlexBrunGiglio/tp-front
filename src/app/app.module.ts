import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LeftContainerComponent } from './left-container/left-container.component';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { TableComponent } from './table/table.component';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component';
import { MatDividerModule } from '@angular/material/divider';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ChartsComponent } from './charts/charts.component';
import { NavComponent } from './nav/nav.component';
import { ChartsModule, WavesModule } from 'angular-bootstrap-md'
import { MatTabsModule } from '@angular/material/tabs';
import { JsonDialogComponent } from './json-dialog/json-dialog.component';
import { AgmCoreModule } from '@agm/core';
import { MapViewComponent } from './map-view/map-view.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LeftContainerComponent,
    TableComponent,
    DialogComponent,
    ChartsComponent,
    NavComponent,
    JsonDialogComponent,
    MapViewComponent,
  ],
  imports: [
    BrowserModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCe4AKdcc0CCt2OwjLW0V-JIxifEjEPUbo'
    }),
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatInputModule,
    FormsModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatDividerModule,
    MatCheckboxModule,
    ChartsModule,
    WavesModule,
    MatTabsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
