import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../data-api.service';
import { People } from '../people';
@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss']
})
export class ChartsComponent implements OnInit {
  peoples: People[] = [];

  chartType: string = 'pie';
  chartBatonType: string = 'bar';
  chartOptions: any = {
    responsive: true,
  };

  statGender: Array<any> = [{ data: [] }];
  statGenderLabel: Array<any> = ['Homme', 'Femme'];

  statCountries: Array<any> = [{ data: [] }];
  statCountriesLabel: Array<any> = [];

  statFruits: Array<any> = [{ data: [] }];
  statFruitsLabel: Array<any> = [];

  statColors: Array<any> = [{ data: [] }];
  statColorsLabel: Array<any> = [];

  statPets: Array<any> = [{ data: [] }];
  statPetsLabel: Array<any> = [];

  public chartColors: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
      // hoverBackgroundColor: ['#FF5A5E', '#5AD3D1', '#FFC870', '#A8B3C5', '#616774'],
      borderWidth: 2,
    }
  ];

  public chartColorsCustom: Array<any> = [
    {
      backgroundColor: ['#EFA825', '#EFEF2F', '#2FAFEF', '#4CEF2F', '#EF2F2F', '#AF2FEF'],
      // hoverBackgroundColor: ['#FF5A5E', '#5AD3D1', '#FFC870', '#A8B3C5', '#616774'],
      borderWidth: 2,
    }
  ];

  constructor(
    private apiService: DataApiService,
  ) { }

  ngOnInit(): void {
    this.init();
    this.setRandomColor();
  }

  async init() {
    const response = await this.apiService.getPeoples().toPromise();
    this.peoples = response.people;
    this.setGenderStats();
    this.setCountryStats();
    this.setFruitStats();
    this.setColorStats();
    this.setPetsStats();
  }

  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

  setRandomColor() {
    for (let i = 0; i < 50; i++) {
      const color = "#" + ((1 << 24) * Math.random() | 0).toString(16)
      this.chartColors[0].backgroundColor.push(color)
    }
  }

  setGenderStats() {
    const numberOfFemale = this.peoples.filter(x => x.gender === "Female").length;
    const numberOfMale = this.peoples.filter(x => x.gender === "Male").length;
    this.statGender[0].data.push(numberOfFemale, numberOfMale)
  }

  setCountryStats() {
    const distinct = (value, index, self) => {
      return self.indexOf(value) === index;
    }

    const distinctCountries = this.peoples.map(x => x.contact.country).filter(distinct);

    for (const country of distinctCountries) {
      const getAllMembersOfCountry = this.peoples.filter(x => x.contact.country === country).length;
      this.statCountries[0].data.push(getAllMembersOfCountry);
      this.statCountriesLabel.push(country);
    }
  }

  setFruitStats() {
    const distinct = (value, index, self) => {
      return self.indexOf(value) === index;
    }

    const distinctFruits = this.peoples.map(x => x.preferences.favorite_fruit).filter(distinct);

    for (const fruit of distinctFruits) {
      const getAllFavFruit = this.peoples.filter(x => x.preferences.favorite_fruit === fruit).length;
      this.statFruits[0].data.push(getAllFavFruit);
      this.statFruitsLabel.push(fruit);
    }
  }

  setColorStats() {
    const distinct = (value, index, self) => {
      return self.indexOf(value) === index;
    }

    const distinctColors = this.peoples.map(x => x.preferences.favorite_color).filter(distinct);

    for (const color of distinctColors) {
      const getAllFavColor = this.peoples.filter(x => x.preferences.favorite_color === color).length;
      this.statColors[0].data.push(getAllFavColor);
      this.statColorsLabel.push(color);
    }
  }

  setPetsStats() {
    const distinct = (value, index, self) => {
      return self.indexOf(value) === index;
    }

    const distincPets = this.peoples.map(x => x.preferences.favorite_pet).filter(distinct);

    for (const pet of distincPets) {
      const getAllFavPet = this.peoples.filter(x => x.preferences.favorite_pet === pet).length;
      this.statPets[0].data.push(getAllFavPet);
      this.statPetsLabel.push(pet);
    }
    this.statPets[0].data.push(0);
  }
}
