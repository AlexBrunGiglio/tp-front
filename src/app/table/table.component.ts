import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { DataApiService } from '../data-api.service';
import { DialogComponent } from '../dialog/dialog.component';
import { JsonDialogComponent } from '../json-dialog/json-dialog.component';
import { People } from '../people';

@Component({
  selector: 'app-table-component',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  peoples: People[] = [];
  displayedColumns: string[] = ['id', 'firstname', 'lastname', 'gender', 'actions'];
  dataSource: MatTableDataSource<People>;
  filterGender: 'Female' | 'Male' | 'All';
  filterColor: 'Orange' | 'Blue' | 'Yellow' | 'Green' | 'Red' | 'Purple';

  fruitList: string[];
  filterFruit: string;

  petList: string[];
  filterPet: string;

  countryList: string[];
  filterCountry: string;

  cityList: string[];
  filterCity: string;

  distinct = (value, index, self) => {
    return self.indexOf(value) === index;
  }

  listToExport: People[] = [];
  constructor(
    private apiService: DataApiService,
    public dialogService: MatDialog,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.init();
  }

  async init() {
    const response = await this.apiService.getPeoples().toPromise();
    this.peoples = response.people;
    this.loadData(this.peoples);
    this.getAllData();
  }

  loadData(data: People[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.listToExport = data;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialog(people: People) {
    const dialogRef = this.dialogService.open(DialogComponent, { data: people });
    // dialogRef.afterClosed().subscribe(result => {
    // });
  }

  filteredChange() {
    let peoplesFiltered = this.peoples;
    if (this.filterGender) {
      peoplesFiltered = peoplesFiltered.filter(x => x.gender === this.filterGender);
      this.loadData(peoplesFiltered);
    }
    if (this.filterColor) {
      peoplesFiltered = peoplesFiltered.filter(x => x.preferences.favorite_color === this.filterColor);
      this.loadData(peoplesFiltered);
    }
    if (this.filterFruit) {
      peoplesFiltered = peoplesFiltered.filter(x => x.preferences.favorite_fruit === this.filterFruit);
      this.loadData(peoplesFiltered);
    }
    if (this.filterPet) {
      peoplesFiltered = peoplesFiltered.filter(x => x.preferences.favorite_pet === this.filterPet);
      this.loadData(peoplesFiltered);
    }

    if (this.filterCountry) {
      peoplesFiltered = peoplesFiltered.filter(x => x.contact.country === this.filterCountry);
      this.loadData(peoplesFiltered);
      const distinctCity = peoplesFiltered.map(x => x.contact.city).filter(this.distinct);
      this.cityList = distinctCity;
    }

    if (this.filterCity) {
      peoplesFiltered = peoplesFiltered.filter(x => x.contact.city === this.filterCity && x.contact.country === this.filterCountry);
      this.loadData(peoplesFiltered);
    }

    this.listToExport = peoplesFiltered;
  }

  getAllData() {
    const distinctFruits = this.peoples.map(x => x.preferences.favorite_fruit).filter(this.distinct);
    this.fruitList = distinctFruits;

    const distinctPets = this.peoples.map(x => x.preferences.favorite_pet).filter(this.distinct);
    this.petList = distinctPets;

    const distinctCountry = this.peoples.map(x => x.contact.country).filter(this.distinct);
    this.countryList = distinctCountry;
  }

  exportToJson() {
    const arrToString = JSON.stringify(this.listToExport);
    const jsonObject = JSON.parse(arrToString);
    const dialogRef = this.dialogService.open(JsonDialogComponent, { data: jsonObject });
  }

  resetFilter() {
    if (this.filterGender) {
      this.filterGender = null;
    }
    if (this.filterColor) {
      this.filterColor = null;
    }
    if (this.filterFruit) {
      this.filterFruit = null;
    }
    if (this.filterPet) {
      this.filterPet = null;
    }
    if (this.filterCountry) {
      this.filterCountry = null;
    }
    if (this.filterCity) {
      this.filterCity = null;
    }
    this.loadData(this.peoples);
  }
}
